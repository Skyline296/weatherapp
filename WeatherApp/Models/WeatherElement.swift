//
//  WeatherElement.swift
//  WeatherApp
//
//  Created by Skyline on 24/09/2021.
//

import Foundation

struct WeatherElement: Codable {
	let main: String
	let description: String
	let icon: String
}
