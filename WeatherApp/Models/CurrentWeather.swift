//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by Skyline on 24/09/2021.
//

import Foundation

struct CurrentWeather: Codable, Identifiable {
	var id = UUID()
	let dt: Int
	let temp: Double
	let feelsLike: Double
	let pressure: Int
	let humidity: Int
	let windSpeed: Double
	let windDeg: Int
	let windGust: Double?
	let weatherElement: [WeatherElement]
	let pop: Double?

	enum CodingKeys: String, CodingKey {
		case dt
		case temp
		case feelsLike = "feels_like"
		case pressure
		case humidity
		case windSpeed = "wind_speed"
		case windDeg = "wind_deg"
		case windGust = "wind_gust"
		case weatherElement = "weather"
		case pop
	}

	init() {
		id = UUID()
		dt = 0
		temp = 0
		feelsLike = 0
		pressure = 0
		humidity = 0
		windSpeed = 0
		windDeg = 0
		weatherElement = [WeatherElement(main: "", description: "", icon: "")]
		windGust = 0
		pop = 0
	}
}
