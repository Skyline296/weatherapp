//
//  Weather.swift
//  WeatherApp
//
//  Created by Skyline on 16/09/2021.
//

import Foundation

struct Weather: Codable {
    let current: CurrentWeather
	let hourly: [CurrentWeather]
    let daily: [DailyWeather]

	static func empty() -> Weather {
		return Weather(
			current: CurrentWeather(),
			hourly: [CurrentWeather](repeating: CurrentWeather(), count: 1),
			daily: [DailyWeather](repeating: DailyWeather(), count: 8))
	}
}
