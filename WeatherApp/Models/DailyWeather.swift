//
//  DailyWeather.swift
//  WeatherApp
//
//  Created by Skyline on 24/09/2021.
//

import Foundation

struct DailyWeather: Codable {
	let dt: Int
	let sunrise: Int
	let sunset: Int
	let temp: Temperature
	let pressure: Int
	let humidity: Int
	let windSpeed: Double
	let windDeg: Int
	let windGust: Double
	let weatherElement: [WeatherElement]
	let pop: Double


	enum CodingKeys: String, CodingKey {
		case dt
		case sunrise
		case sunset
		case temp
		case pressure
		case humidity
		case windSpeed = "wind_speed"
		case windDeg = "wind_deg"
		case windGust = "wind_gust"
		case weatherElement = "weather"
		case pop
	}

	init() {
		dt = 0
		sunrise = 0
		sunset = 0
		temp = Temperature(day: 0, min: 0, max: 0, night: 0, eve: 0, morn: 0)
		pressure = 0
		humidity = 0
		windSpeed = 0
		windDeg = 0
		windGust = 0
		weatherElement = [WeatherElement(main: "", description: "", icon: "")]
		pop = 0
	}
}
