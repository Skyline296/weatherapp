//
//  Temp.swift
//  WeatherApp
//
//  Created by Skyline on 24/09/2021.
//

import Foundation

struct Temperature: Codable {
	let day: Double
	let min: Double
	let max: Double
	let night: Double
	let eve: Double
	let morn: Double
}
