//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Skyline on 22/09/2021.
//

import Foundation
import Combine
import CoreLocation

class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
	private let locationManager: CLLocationManager
	@Published var location: CLLocation?
	@Published var placemark: CLPlacemark?
	var objectWillChange: PassthroughSubject<String, Never> = .init()

	override init() {
		locationManager = CLLocationManager()

	super.init()
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
	}

	func getLocation() {
		locationManager.requestWhenInUseAuthorization()
		locationManager.startUpdatingLocation()
	}

	func locationManager(_ manager: CLLocationManager,
						 didUpdateLocations locations: [CLLocation]) {
		guard let location = locations.first else { return }
		self.location = location
		fetchCity(with: location)
		locationManager.stopUpdatingLocation()
	}

	func fetchCity(with location: CLLocation) {
		CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
			if let error = error { print(error.localizedDescription) }
			let city = placemarks?.first?.locality ?? ""
			self.objectWillChange.send(city)
		}
	}
}
