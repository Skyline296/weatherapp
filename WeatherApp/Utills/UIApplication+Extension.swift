//
//  UIApplication+Extension.swift
//  WeatherApp
//
//  Created by Skyline on 26/09/2021.
//

import SwiftUI
import Foundation

extension UIApplication {
	func addTapGestureListener() {
		guard let area = windows.first else { return }
		let tap = UITapGestureRecognizer(target: area, action: #selector(UIView.endEditing))
		tap.requiresExclusiveTouchType = false
		tap.cancelsTouchesInView = false
		tap.delegate = self
		area.addGestureRecognizer(tap)
	}
}

extension UIApplication: UIGestureRecognizerDelegate {
	public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		return true
	}
}
