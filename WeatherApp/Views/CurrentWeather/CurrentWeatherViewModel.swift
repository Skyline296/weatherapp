//
//  MainViewModel.swift
//  WeatherApp
//
//  Created by Skyline on 17/09/2021.
//

import Foundation
import Combine
import SwiftUI
import CoreLocation

class CurrentWeatherViewModel: ObservableObject {
	private var weatherService: WeatherService
	private var locationManager: LocationManager
	@AppStorage("city") var city: String = "Locating..."
	private var cities = ["Warsaw", "London", "New York", "Tokyo", "Vienna", "Dubai", "Kyiv"]

	@Published var weather = Weather.empty()
	@Published var hourlyCellViewModels = [HourlyWeatherCellViewModel]()
	@Published var searchingCity = ""
	@Published var isLoadingData = true
	@Published var isAlertPresented = false
	@Published var isSearchCityPresented = false
	@Published var isSearchDataValid = false
	@Published var errorMessage = ""

	private var cancellables = Set<AnyCancellable>()

	init() {
		self.locationManager = LocationManager()
		self.weatherService = WeatherService()

		locationManager.$location
			.receive(on: RunLoop.main)
			.sink { [weak self] location in
				guard location != nil else { return }
				self?.getWeather(self?.lat ?? "\(0.0)", self?.lon ?? "\(0.0)")
			}
			.store(in: &cancellables)

		locationManager.objectWillChange
			.receive(on: RunLoop.main)
			.sink { _ in } receiveValue: { [weak self] city in
				self?.city = city
			}
			.store(in: &cancellables)

		$weather.map { weathers in
			weathers.hourly.map { hourly in
				HourlyWeatherCellViewModel(currentWeather: hourly)
			}
		}
		.assign(to: \.hourlyCellViewModels, on: self)
		.store(in: &cancellables)

		isSearchTextValid
			.receive(on: RunLoop.main)
			.assign(to: \.isSearchDataValid, on: self)
			.store(in: &cancellables)

		if city == "Locating..." {
			updateUserLocation()
		} else {
			getWeatherForCity(with: city)
		}
	}

	func getWeather(_ lat: String, _ lon: String) {
		isLoadingData = true
		weatherService.loadWeatherData(endpoint: .makeUrlQuery(
				[ "lat": "\(lat)",
				  "lon": "\(lon)",
				  "units": "metric",
				  "exclude": "minutely,alerts"
				]))
			.receive(on: RunLoop.main)
			.sink { completion in
				switch completion {
					case .failure(let error):
						self.isLoadingData = false
						self.presentErrorAlert(error)
					case .finished:
							self.isLoadingData = false
				}
			} receiveValue: { [weak self] weather in
				self?.weather = weather
			}
			.store(in: &cancellables)
	}

	func getWeatherForCity(with city: String) {
		CLGeocoder().geocodeAddressString(city) { placemarks, error in
			if error != nil {
				self.presentErrorAlert(.invalidCity)
			}
			if let lat = placemarks?.first?.location?.coordinate.latitude,
			   let lon = placemarks?.first?.location?.coordinate.longitude {
				self.getWeather("\(lat)", "\(lon)")
				self.city = city
			}
		}
	}

	func updateUserLocation() {
		locationManager.getLocation()
	}

	private var isSearchTextValid: AnyPublisher<Bool, Never> {
		$searchingCity
			.debounce(for: 0.8, scheduler: RunLoop.main)
			.map { text in
				return text.count > 3
			}
			.eraseToAnyPublisher()
	}

	private static var numFormatter: NumberFormatter {
		let formatter = NumberFormatter()
		formatter.maximumFractionDigits = 0
		return formatter
	}

	private static var numPercentFormatter: NumberFormatter {
		let formatter = NumberFormatter()
		formatter.numberStyle = .percent
		return formatter
	}

	var lat: String {
		return "\(locationManager.location?.coordinate.latitude ?? 0.0)"
	}

	var lon: String {
		return "\(locationManager.location?.coordinate.longitude ?? 0.0)"
	}

	var temp: String {
		return "\(Self.numFormatter.string(for: weather.current.temp) ?? "0")°"
	}

	private var tempHign: String {
		return Self.numFormatter.string(for: weather.daily[0].temp.max) ?? "0"
	}

	private var tempLow: String {
		return Self.numFormatter.string(for: weather.daily[0].temp.min) ?? "0"
	}

	var tempHighAndLow: String {
		return "H: \(tempHign)°, L: \(tempLow)°"
	}

	var description: String {
		return weather.current.weatherElement[0].description.capitalized
	}

	var fillsLike: String {
		return "Feels like \(Self.numFormatter.string(for: weather.current.feelsLike) ?? "0")°"
	}

	var pop: String {
		return Self.numPercentFormatter.string(for: weather.hourly[0].pop) ?? "0"
	}

	var humidity: String {
		return "\(weather.current.humidity)%"
	}

	var wind: String {
		return "\(weather.current.windSpeed) m/s"
	}

	var maxWindGusts: String {
		return "\(weather.current.windGust ?? 0) m/s"
	}

	var pressure: String {
		return "\(weather.current.pressure) hPa"
	}

	var icon: String {
		return "https://openweathermap.org/img/wn/\(weather.current.weatherElement[0].icon)@2x.png"
	}

	var searchResults: [String] {
		if searchingCity.isEmpty {
			return cities
		} else {
			return cities.filter { $0.contains(searchingCity) }
		}
	}

	func presentErrorAlert(_ error: WeatherServiceError) {
		DispatchQueue.main.async {
			self.errorMessage = error.errorDescription ?? "Unknown error"
			self.isAlertPresented = true
		}
	}
}
