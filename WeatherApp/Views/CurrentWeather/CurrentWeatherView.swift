//
//  ContentView.swift
//  WeatherApp
//
//  Created by Skyline on 16/09/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct CurrentWeatherView: View {
	@ObservedObject var currentWeatherVM = CurrentWeatherViewModel()

    var body: some View {
		NavigationView {
			if currentWeatherVM.isLoadingData {
				VStack {
					ProgressView().scaleEffect(1.5)
						.padding(.bottom, 5)

					Text("Loading data...")
						.font(.subheadline)
						.fontWeight(.regular)
				}
				.navigationTitle("Weather")
			} else {
			ScrollView(showsIndicators: false) {
				VStack {
					VStack {
						VStack {
							Text(currentWeatherVM.city)
								.font(.system(size: 24,
									weight: .medium,
									design: .rounded))
								.minimumScaleFactor(0.5)
								.lineLimit(1)

							HStack(spacing: 0) {
								Text(currentWeatherVM.temp)
								WebImage(url: URL(string: currentWeatherVM.icon))
									.resizable()
									.aspectRatio(contentMode: .fit)
									.frame(minWidth: 0,
										   maxWidth: 50,
										   maxHeight: 50,
										   alignment: .center)
									.shadow(radius: 5)
							}
							.font(.system(size: 36,
								weight: .medium,
								design: .rounded))	
						}
						.padding([.top, .horizontal])

						HStack(alignment: .center) {
							Text(currentWeatherVM.tempHighAndLow)
							Circle()
								.frame(width: 6, height: 6)
							Text(currentWeatherVM.description)
							Circle()
								.frame(width: 6, height: 6)
							Text(currentWeatherVM.fillsLike)
						}
						.opacity(0.65)
						.minimumScaleFactor(0.5)
						.lineLimit(1)
						.font(.system(size: 16,
							weight: .regular,
							design: .rounded))
						.padding(.horizontal)

						VStack {
							Divider()
							HStack {
								Text("Chance of rain")
								Spacer()
								Text(currentWeatherVM.pop)
									.fontWeight(.medium)
							}
							.padding(.vertical, 2)
							Divider()
							HStack {
								Text("Humidity")
								Spacer()
								Text(currentWeatherVM.humidity)
									.fontWeight(.medium)
							}
							.padding(.vertical, 2)
							Divider()
							HStack {
								Text("Wind")
									.font(.system(size: 16,
									weight: .bold,
									design: .rounded))
							Spacer()
								Text(currentWeatherVM.wind)
									.fontWeight(.medium)
							}
							.padding(.vertical, 2)
							Divider()
							HStack {
								Text("Max Wind Gusts")
								Spacer()
								Text(currentWeatherVM.maxWindGusts)
									.fontWeight(.medium)
							}
							.padding(.vertical, 2)
							Divider()
							HStack {
								Text("Pressure")
								Spacer()
								Text(currentWeatherVM.pressure)
									.fontWeight(.medium)
							}
							.padding(.top, 2)
						}
						.font(.system(size: 16,
									  weight: .bold,
									  design: .rounded))
						.padding([.horizontal, .bottom])
					}
					.background(Color("CardBackgroundColor"))
					.cornerRadius(15)
					.shadow(radius: 5)
					.padding([.top, .horizontal])

					ScrollView(.horizontal, showsIndicators: true) {
						LazyHStack(alignment: .center, spacing: 15) {
							ForEach(currentWeatherVM.hourlyCellViewModels) { hourlyCellVM in
								HourlyWeatherCell(hourlyWeatherCellVM: hourlyCellVM)
							}
						}
						.padding(.horizontal)
					}
					Spacer()
					NavigationLink(destination: DailyWeatherView(
												dailyWeatherVM: DailyWeatherViewModel(
												weather: currentWeatherVM.weather.daily)
					)) {
						HStack {
							Text("7-day forecast")
								.font(.system(size: 21,
											  weight: .bold,
											  design: .rounded))
								.padding(12)
						}
						.frame(minWidth: 0,
							   maxWidth: .infinity,
							   alignment: .center)
						.foregroundColor(.white)
						.background(Color.blue)
						.cornerRadius(15)
						.shadow(radius: 5)
					}
					.padding([.horizontal, .bottom])
				}
				.navigationBarTitle("Weather")
				.alert(isPresented: $currentWeatherVM.isAlertPresented) {
					Alert(
						title: Text("Error!"),
						message: Text(currentWeatherVM.errorMessage),
						dismissButton: .default(Text("OK")) { currentWeatherVM.errorMessage = "" }
					)
				}
				.toolbar {
					ToolbarItem(placement: .navigationBarTrailing) {
						Button {
							currentWeatherVM.isSearchCityPresented.toggle()
						} label: {
							Image(systemName: "line.horizontal.3.decrease.circle.fill")
						}
					}
				}
				.sheet(isPresented: $currentWeatherVM.isSearchCityPresented) {
					NavigationView {
						VStack {
						if #available(iOS 15.0, *) {
							if !currentWeatherVM.isSearchDataValid {
								Button(action: {
									currentWeatherVM.updateUserLocation()
									currentWeatherVM.isSearchCityPresented.toggle()
								}) {
									HStack {
										Image(systemName: "location.fill")
										Text("Current location")
											.font(.system(size: 16,
														  weight: .semibold,
														  design: .rounded))
											.padding(10)
									}
									.frame(minWidth: 0,
										   maxWidth: .infinity,
										   alignment: .center)
									.foregroundColor(.white)
									.background(Color.blue)
									.cornerRadius(15)
									.padding(.horizontal)
								}
								} else {
									Button(action: {
										currentWeatherVM.getWeatherForCity(with: currentWeatherVM.searchingCity)
										currentWeatherVM.isSearchCityPresented.toggle()
									}) {
										HStack {
											Image(systemName: "location.fill")
											Text("Get Weather")
												.font(.system(size: 16,
															  weight: .semibold,
															  design: .rounded))
												.padding(10)
										}
										.frame(minWidth: 0,
											   maxWidth: .infinity,
											   alignment: .center)
										.foregroundColor(.white)
										.background(Color.green)
										.cornerRadius(15)
										.padding(.horizontal)
									}
								}
							List {
								ForEach(currentWeatherVM.searchResults, id: \.self) { city in
									Button(city,
										   action: {
										currentWeatherVM.getWeatherForCity(with: city)
										currentWeatherVM.isSearchCityPresented.toggle()})
								}
							}
							.listStyle(InsetGroupedListStyle())
							.searchable(text: $currentWeatherVM.searchingCity,
										prompt: "Search for a city..") // New IOS 15 feature
						} else {
						VStack {
							ZStack {
								Rectangle()
									.foregroundColor(Color(.systemGray6))
								HStack {
									Image(systemName: "magnifyingglass")
									TextField("Search for a city..", text: $currentWeatherVM.searchingCity)
								}
								.padding(.leading, 13)
							}
							.frame(height: 40)
							.cornerRadius(15)
							.padding()

							if !currentWeatherVM.isSearchDataValid {
								Button(action: {
									currentWeatherVM.updateUserLocation()
									currentWeatherVM.isSearchCityPresented.toggle()
								}) {
									HStack {
										Image(systemName: "location.fill")
										Text("Current location")
											.font(.system(size: 16,
														  weight: .semibold,
														  design: .rounded))
											.padding(10)
									}
									.frame(minWidth: 0,
										   maxWidth: .infinity,
										   alignment: .center)
									.foregroundColor(.white)
									.background(Color.blue)
									.cornerRadius(15)
									.padding(.horizontal)
								}
							} else {
								Button(action: {
									currentWeatherVM.getWeatherForCity(with: currentWeatherVM.searchingCity)
									currentWeatherVM.isSearchCityPresented.toggle()
								}) {
									HStack {
										Image(systemName: "location.fill")
										Text("Get Weather")
											.font(.system(size: 16,
														  weight: .semibold,
														  design: .rounded))
											.padding(10)
									}
									.frame(minWidth: 0,
										   maxWidth: .infinity,
										   alignment: .center)
									.foregroundColor(.white)
									.background(Color.green)
									.cornerRadius(15)
									.padding(.horizontal)
								}
							}

							List(currentWeatherVM.searchResults, id: \.self) { city in
								Button(city, action: {
										currentWeatherVM.getWeatherForCity(with: city)
										currentWeatherVM.isSearchCityPresented.toggle()
								})
							}
							.listStyle(InsetGroupedListStyle())
						}
						}
					}
					.navigationTitle("Find city")
					.navigationBarItems(trailing: Button("Cancel", action: {
						currentWeatherVM.isSearchCityPresented.toggle()
					}))
					.font(.system(size: 16,
								  weight: .regular,
								  design: .rounded))
					}
				}
			}
			}
		}
		.navigationViewStyle(StackNavigationViewStyle())
	}
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        CurrentWeatherView()
    }
}
