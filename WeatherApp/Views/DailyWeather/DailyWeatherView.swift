//
//  DailyWeatherView.swift
//  WeatherApp
//
//  Created by Skyline on 25/09/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct DailyWeatherView: View {
	@ObservedObject var dailyWeatherVM: DailyWeatherViewModel
	
    var body: some View {
		ScrollView(showsIndicators: true) {
			VStack(spacing: 15) {
				ForEach(dailyWeatherVM.dailyCellViewModels) { dailyCellVM in
					DailyWeatherCell(dailyWeatherCellVM: dailyCellVM)
				}
			}
			.padding()
		}
		.navigationTitle("7-day forecast")
    }
}

struct DailyWeatherView_Previews: PreviewProvider {
    static var previews: some View {
		DailyWeatherView(dailyWeatherVM: DailyWeatherViewModel(weather: Weather.empty().daily))
    }
}
