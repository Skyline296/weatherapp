//
//  DailyWeatherViewModel.swift
//  WeatherApp
//
//  Created by Skyline on 25/09/2021.
//

import Foundation

import Foundation
import Combine

class DailyWeatherViewModel: ObservableObject {
	@Published var weather: [DailyWeather]
	@Published var dailyCellViewModels = [DailyWeatherCellViewModel]()

	private var cancellables = Set<AnyCancellable>()

	init(weather: [DailyWeather]) {
		self.weather = weather
		$weather.map { weather in
			weather.map { weather in
				DailyWeatherCellViewModel(dailyWeather: weather)
			}
		}
		.assign(to: \.dailyCellViewModels, on: self)
		.store(in: &cancellables)
	}
}
