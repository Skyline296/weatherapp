//
//  HourlyView.swift
//  WeatherApp
//
//  Created by Skyline on 21/09/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct HourlyWeatherCell: View {
	@ObservedObject var hourlyWeatherCellVM: HourlyWeatherCellViewModel

    var body: some View {
		VStack(alignment: .center) {
				Spacer()
			Text(hourlyWeatherCellVM.time)
				.font(.system(size: 16,
							  weight: .medium,
							  design: .rounded))
				.minimumScaleFactor(0.5)
				.lineLimit(1)
			Spacer()
			HStack(spacing: 0) {
				Text(hourlyWeatherCellVM.temp)
					.font(.system(size: 16,
								  weight: .medium,
								  design: .rounded))
					.fontWeight(.bold)
					.minimumScaleFactor(0.5)
					.lineLimit(1)
				WebImage(url: URL(string: hourlyWeatherCellVM.icon))
					.resizable()
					.aspectRatio(contentMode: .fit)
					.frame(minWidth: 0, maxWidth: 35, maxHeight: 35, alignment: .center)
					.shadow(radius: 5)
			}
			Spacer()
			VStack {
				Text(hourlyWeatherCellVM.description)
					.font(.system(size: 14,
						weight: .regular,
						design: .rounded))
					.opacity(0.65)
					.minimumScaleFactor(0.5)
					.multilineTextAlignment(.center)
					.lineLimit(2)
			}
			Spacer()
		}
		.padding(.horizontal)
		.frame(minWidth: 105,
			   maxWidth: 105,
			   minHeight: 150,
			   maxHeight: .infinity)
		.background(Color("CardBackgroundColor"))
		.cornerRadius(15)
		.shadow(radius: 5)
		.padding(.vertical)
    }
}
