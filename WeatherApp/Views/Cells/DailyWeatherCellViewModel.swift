//
//  DailyWeatherCellViewModel.swift
//  WeatherApp
//
//  Created by Skyline on 25/09/2021.
//

import Foundation

class DailyWeatherCellViewModel: ObservableObject, Identifiable {
	@Published var dailyWeather: DailyWeather

	init(dailyWeather: DailyWeather) {
		self.dailyWeather = dailyWeather
	}

	private static var numFormatter: NumberFormatter {
		let formatter = NumberFormatter()
		formatter.maximumFractionDigits = 0
		return formatter
	}

	private static var dateFormatter: DateFormatter {
		let formatter = Foundation.DateFormatter()
		formatter.dateFormat = "E, d MMM"
		return formatter
	}

	var date: String {
		return Self.dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(dailyWeather.dt)))
	}

	var temp: String {
		return "\(Self.numFormatter.string(for: dailyWeather.temp.day) ?? "0")°"
	}

	private var tempHign: String {
		return Self.numFormatter.string(for: dailyWeather.temp.max) ?? "0"
	}

	private var tempLow: String {
		return Self.numFormatter.string(for: dailyWeather.temp.min) ?? "0"
	}

	var tempHighAndLow: String {
		return "H: \(tempHign)°, L: \(tempLow)°"
	}

	var icon: String {
		return "https://openweathermap.org/img/wn/\(dailyWeather.weatherElement[0].icon)@2x.png"
	}

	var description: String {
		return dailyWeather.weatherElement[0].description.capitalized
	}
}
