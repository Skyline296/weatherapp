//
//  DailyWeatherCell.swift
//  WeatherApp
//
//  Created by Skyline on 25/09/2021.
//

import SwiftUI
import SDWebImageSwiftUI

struct DailyWeatherCell: View {
	@ObservedObject var dailyWeatherCellVM: DailyWeatherCellViewModel

    var body: some View {
		HStack {
			Text(dailyWeatherCellVM.date)
				.font(.system(size: 16,
							  weight: .medium,
							  design: .rounded))
				.minimumScaleFactor(0.5)
				.lineLimit(1)
			Spacer()
			HStack(spacing: 0) {
				Text(dailyWeatherCellVM.temp)
					.font(.system(size: 18,
								  weight: .medium,
								  design: .rounded))
					.fontWeight(.bold)
					.minimumScaleFactor(0.5)
					.lineLimit(1)
				WebImage(url: URL(string: dailyWeatherCellVM.icon))
					.resizable()
					.aspectRatio(contentMode: .fit)
					.frame(minWidth: 0, maxWidth: 40, maxHeight: 40, alignment: .center)
					.shadow(radius: 5)
			}
			Spacer()
			VStack(alignment: .trailing, spacing: 5) {
				Text(dailyWeatherCellVM.tempHighAndLow)
				Text(dailyWeatherCellVM.description)
			}
			.font(.system(size: 14,
						  weight: .regular,
						  design: .rounded))
			.opacity(0.65)
			.minimumScaleFactor(0.5)
			.multilineTextAlignment(.trailing)
			.lineLimit(1)
		}
		.navigationBarTitleDisplayMode(.inline)
		.padding(.horizontal)
		.frame(minWidth:0,
			   maxWidth: .infinity,
			   minHeight: 90,
			   maxHeight: .infinity)
		.background(Color("CardBackgroundColor"))
		.cornerRadius(15)
		.shadow(radius: 5)
    }
}

struct DailyWeatherCell_Previews: PreviewProvider {
    static var previews: some View {
		DailyWeatherCell(dailyWeatherCellVM: DailyWeatherCellViewModel(
			dailyWeather: Weather.empty()
				.daily[0])
		)
    }
}
