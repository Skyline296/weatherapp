//
//  HourlyWeatherCellViewModel.swift
//  WeatherApp
//
//  Created by Skyline on 25/09/2021.
//

import Foundation
import Combine

class HourlyWeatherCellViewModel: ObservableObject, Identifiable {
	@Published var currentWeather: CurrentWeather

	init(currentWeather: CurrentWeather) {
		self.currentWeather = currentWeather
	}

	private static var numFormatter: NumberFormatter {
		let formatter = NumberFormatter()
		formatter.maximumFractionDigits = 0
		return formatter
	}

	private static var dateFormatter: DateFormatter {
		let formatter = Foundation.DateFormatter()
		formatter.dateFormat = "HH:mm"
		return formatter
	}

	var time: String {
		return Self.dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(currentWeather.dt)))
	}

	var temp: String {
		return "\(Self.numFormatter.string(for: currentWeather.temp) ?? "0")°"
	}

	var icon: String {
		return "https://openweathermap.org/img/wn/\(currentWeather.weatherElement[0].icon)@2x.png"
	}

	var description: String {
		return currentWeather.weatherElement[0].description.capitalized
	}
}
