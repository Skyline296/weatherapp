//
//  WeatherService.swift
//  WeatherApp
//
//  Created by Skyline on 16/09/2021.
//

import Foundation
import Combine

enum WeatherServiceError: Error {
	case unauthorized
	case serverError
	case wrongUrl
	case invalidCity

	var errorDescription: String? {
		switch self {
		case .unauthorized:
			return "Unauthorized, please check api key"
		case .serverError:
			return "Server error, please try again"
		case .wrongUrl:
			return "Invalid URL address"
		case .invalidCity:
			return "Can't find city, please try again"
		}
	}
}

class WeatherService {

	func loadWeatherData(endpoint: Endpoint) -> AnyPublisher<Weather, WeatherServiceError> {
		guard let url = endpoint.url else {
			return Fail(error: WeatherServiceError.wrongUrl).eraseToAnyPublisher()
		}
		return
			URLSession
			.shared
			.dataTaskPublisher(for: url)
			.tryMap { try self.getData(from: $0) }
			.decode(type: Weather.self, decoder: JSONDecoder())
			.mapError { $0 as? WeatherServiceError ?? .serverError }
			.eraseToAnyPublisher()
	}

	private func getData(from response: URLSession.DataTaskPublisher.Output) throws -> Data {
		   let httpResponse = response.response as? HTTPURLResponse

		   switch httpResponse?.statusCode {
		   case 200:
			   return response.data
		   case 401:
			   throw WeatherServiceError.unauthorized
		   default:
			   throw WeatherServiceError.serverError
		   }
	   }
}
