//
//  Endpoint.swift
//  WeatherApp
//
//  Created by Skyline on 19/09/2021.
//

import Foundation

struct Endpoint {
	let path: String
	let queryItems: [URLQueryItem]
}

extension Endpoint {
	static func makeUrlQuery(_ query: [String: String]) -> Endpoint {
		return Endpoint(
			path: "/data/2.5/onecall",
			queryItems: query.map { URLQueryItem(name: $0.key, value: $0.value) }
		)
	}
}

extension Endpoint {
	var url: URL? {
		var components = URLComponents()
		components.scheme = "https"
		components.host = "api.openweathermap.org"
		components.path = path
		components.queryItems = queryItems
		components.queryItems?.append(
			URLQueryItem(name: "appid",
				value: "9f8664804cb2a5500e989cdf0130bde4")
		)
		return components.url
	}
}
