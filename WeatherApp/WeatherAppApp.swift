//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by Skyline on 16/09/2021.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    var body: some Scene {
        WindowGroup {
            CurrentWeatherView().onAppear(perform: UIApplication.shared.addTapGestureListener)
        }
    }
}
